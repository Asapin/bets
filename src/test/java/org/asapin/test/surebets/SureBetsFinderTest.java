package org.asapin.test.surebets;

import org.asapin.bets.bet.SureBetsFinder;
import org.asapin.bets.dao.BetTypeDao;
import org.asapin.bets.dao.GameDao;
import org.asapin.bets.dao.TeamDao;
import org.asapin.bets.domain.Bet;
import org.asapin.bets.domain.SureBet;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.util.StringUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.when;

/**
 * Created by Asapin on 2016-04-13.
 */
public class SureBetsFinderTest {

    @InjectMocks
    private SureBetsFinder sureBetsFinder;

    @Mock
    private TeamDao teamDao;

    @Mock
    private GameDao gameDao;

    @Mock
    private BetTypeDao betTypeDao;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    public Bet readBetFromFile(String file) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(this.getClass().getResourceAsStream(file)));
        String line = br.readLine();
        br.close();
        String[] lineParts = line.split(";");

        Bet bet = new Bet();
        bet.setGameId(Long.parseLong(lineParts[0]));
        bet.setBetTypeId(Long.parseLong(lineParts[1]));
        bet.setGameTime(ZonedDateTime.from(DateTimeFormatter.ISO_ZONED_DATE_TIME.parse(lineParts[2])));
        bet.setBookmakerUrl(lineParts[3]);
        bet.setCoef1(Double.parseDouble(lineParts[4]));
        bet.setCoef2(Double.parseDouble(lineParts[5]));
        bet.setTeam1Id(Integer.parseInt(lineParts[6]));
        bet.setTeam2Id(Integer.parseInt(lineParts[7]));

        return bet;
    }

    public List<SureBet> readSureBetsFromFile(String file) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(this.getClass().getResourceAsStream(file)));
        List<SureBet> sureBets = new ArrayList<>();

        for(String line; (line = br.readLine()) != null; ) {
            String[] lineParts = line.split(";");

            SureBet sureBet = new SureBet();

            sureBet.setGame(lineParts[0]);
            sureBet.setBetType(lineParts[1]);
            sureBet.setLocalTimeString(lineParts[2]);
            sureBet.setBookmaker1Url(lineParts[3]);
            sureBet.setBookmaker2Url(lineParts[4]);
            sureBet.setSureBetTeam1(lineParts[5]);
            sureBet.setSureBetTeam2(lineParts[6]);
            sureBet.setCoef1(Double.parseDouble(lineParts[7]));
            sureBet.setCoef2(Double.parseDouble(lineParts[8]));

            sureBets.add(sureBet);
        }

        br.close();

        return sureBets;
    }

    public String constructPath(String... parts) {
        return StringUtils.arrayToDelimitedString(parts, "/");
    }

    public void checkResult(List<SureBet> result, List<SureBet> expected) {
        Assert.assertNotNull("Result is null", result);
        Assert.assertNotNull("Expected is null", expected);
        Assert.assertEquals("Expected and result has different size", expected.size(), result.size());
        Assert.assertTrue("Result has bets that are not expected", expected.containsAll(result));
        Assert.assertTrue("Result does not have all expected bets", result.containsAll(expected));
    }

    public void test(String testCase) throws IOException {
        when(teamDao.getTeamNameById(1)).thenReturn("team1");
        when(teamDao.getTeamNameById(2)).thenReturn("team2");
        when(gameDao.getGameNameById(1L)).thenReturn("game");
        when(betTypeDao.getBetTypeNameById(1L)).thenReturn("type");

        Bet bet1 = readBetFromFile(constructPath(testCase, "bet1.txt"));
        Bet bet2 = readBetFromFile(constructPath(testCase, "bet2.txt"));
        List<SureBet> result = sureBetsFinder.getSureBets(bet1, bet2);
        List<SureBet> expected = readSureBetsFromFile(constructPath(testCase, "expected.txt"));

        checkResult(result, expected);
    }

    @Test
    public void testCase01() throws IOException {
        test("testCase01");
    }

    @Test
    public void testCase02() throws IOException {
        test("testCase02");
    }

    @Test
    public void testCase03() throws IOException {
        test("testCase03");
    }

    @Test
    public void testCase04() throws IOException {
        test("testCase04");
    }

    @Test
    public void testCase05() throws IOException {
        test("testCase05");
    }

    @Test
    public void testCase06() throws IOException {
        test("testCase06");
    }

    @Test
    public void testCase07() throws IOException {
        test("testCase07");
    }

    @Test
    public void testCase08() throws IOException {
        test("testCase08");
    }

    @Test
    public void testCase09() throws IOException {
        test("testCase09");
    }

    @Test
    public void testCase10() throws IOException {
        test("testCase10");
    }

    @Test
    public void testCase11() throws IOException {
        test("testCase11");
    }

    @Test
    public void testCase12() throws IOException {
        test("testCase12");
    }

    @Test
    public void testCase13() throws IOException {
        test("testCase13");
    }

    @Test
    public void testCase14() throws IOException {
        test("testCase14");
    }

    @Test
    public void testCase15() throws IOException {
        test("testCase15");
    }

    @Test
    public void testCase16() throws IOException {
        test("testCase16");
    }

    @Test
    public void testCase17() throws IOException {
        test("testCase17");
    }

    @Test
    public void testCase18() throws IOException {
        test("testCase18");
    }

    @Test
    public void testCase19() throws IOException {
        test("testCase19");
    }

    @Test
    public void testCase20() throws IOException {
        test("testCase20");
    }

    @Test
    public void testCase21() throws IOException {
        test("testCase21");
    }

    @Test
    public void testCase22() throws IOException {
        test("testCase22");
    }

    @Test
    public void testCase23() throws IOException {
        test("testCase23");
    }

    @Test
    public void testCase24() throws IOException {
        test("testCase24");
    }

    @Test
    public void testCase25() throws IOException {
        test("testCase25");
    }

    @Test
    public void testCase26() throws IOException {
        test("testCase26");
    }

    @Test
    public void testCase27() throws IOException {
        test("testCase27");
    }

    @Test
    public void testCase28() throws IOException {
        test("testCase28");
    }

    @Test
    public void testCase29() throws IOException {
        test("testCase29");
    }

    @Test
    public void testCase30() throws IOException {
        test("testCase30");
    }

    @Test
    public void testCase31() throws IOException {
        test("testCase31");
    }

    @Test
    public void testCase32() throws IOException {
        test("testCase32");
    }
}
