<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html>

<html>
	<head>
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta charset="UTF-8">
        <meta name="distribution" content="IU">
        <meta name="language" content="English">
        <meta name="rating" content="mature">
        <meta name="referrer" content="never">
        <title>Sure bets searcher</title>
        <script type="text/javascript" src="../js/jquery-2.2.0.min.js"></script>
        <script type="text/javascript" src="../js/update.js"></script>
        <link type="text/css" rel="stylesheet" href="../css/main.css">
    </head>

	<body>
	    <div>
	        <audio id="beepSound">
	            <source src="../audio/beep.mp3"></source>
	        </audio>
	    </div>
	    <div class="content">
	        <div class="controls">
	            <a href="javascript:void(0)" onclick="refreshTeamsNames();" rel="noreferrer">Refresh teams names</a>
	        </div>
            <div class="float-content" id="teams">
            </div>
            <div class="float-content bets" id="sureBets">
            </div>
            <div class="float-content">
                <div id="games">
                </div>
                <div id="betTypes">
                </div>
            </div>
        </div>
	</body>
</html>
