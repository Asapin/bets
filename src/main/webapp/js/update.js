var historySurebets = new Array();

var requestPermission = function() {
    if (window.webkitNotifications && window.webkitNotifications.checkPermission) {
        window.webkitNotifications.requestPermission();
    } else if (window.Notification && window.Notification.requestPermission) {
        window.Notification.requestPermission();
    }
};

var refreshTeamsNames = function() {
    $.ajax({
        url: 'refreshTeamsNames',
        type: 'POST',
        beforeSend: function(xhr) {
            xhr.setRequestHeader("Accept", "application/json");
            xhr.setRequestHeader("Content-Type", "application/json");
        }
    });
};

var notify = function(title, body, tag) {
    var notification;
    if (window.Notification) { /* Safari 6, Chrome (23+) */
        notification =  new window.Notification(title, {
            icon: '',
            body: body + '%',
            tag: tag
        });
    } else if (window.webkitNotifications) {
        notification = window.webkitNotifications.createNotification('', title, body + '%');
        notification.show();
    } else if (navigator.mozNotification) {
        notification = navigator.mozNotification.createNotification(title, body + '%', '');
        notification.show();
    } else if (win.external && win.external.msIsSiteMode()) {
        win.external.msSiteModeClearIconOverlay();
        win.external.msSiteModeSetIconOverlay('', title);
        win.external.msSiteModeActivate();
        notification = {
            "ieVerification": ieVerification + 1
        };
    }
    return notification;
};

var playSound = function() {
    $("#beepSound")[0].play();
};

var getSurebetHtmlString = function(surebet) {
    var gameString = '<tr><td colspan="4">' + surebet.game + '</td></tr>';
    var timeString = '<tr><td colspan="4">' + surebet.localTimeString + '</td></tr>';
    var typeAndProfitString =
        '<tr>' +
            '<td colspan="2">' + surebet.betType + '</td>' +
            '<td colspan="2">Profit: ' + ((1 - (1 / surebet.coef1 + 1 / surebet.coef2)) * 100).toFixed(2) + '%</td>' +
        '</tr>';
    var teamString =
        '<tr>' +
            '<td rowspan="2"><a href="' + surebet.bookmaker1Url + '" target="_blank" rel="noreferrer">' + surebet.sureBetTeam1 + '</a></td>' +
            '<td>' + surebet.coef1 + '</td>' +
            '<td>' + surebet.coef2 + '</td>' +
            '<td rowspan="2"><a href="' + surebet.bookmaker2Url + '" target="_blank" rel="noreferrer">' + surebet.sureBetTeam2 + '</a></td>' +
        '</tr>';
    var betAmountString =
        '<tr>' +
            '<td>' + (1 / surebet.coef1).toFixed(3) + '</td>' +
            '<td>' + (1 / surebet.coef2).toFixed(3) + '</td>' +
        '</tr>';
    var separator = '<tr class="separator"></tr>';
    var htmlString = gameString + timeString + typeAndProfitString +
        teamString + betAmountString + separator;
    return htmlString;
};

var getSurebetId = function(surebet) {
    var surebetId = surebet.game + surebet.localTimeString + surebet.bookmaker1Url +
                    surebet.bookmaker2Url + surebet.sureBetTeam1 + surebet.sureBetTeam2;
    return surebetId;
};

var showSurebetNotification = function(surebet) {
    notify(surebet.sureBetTeam1 + ' - ' + surebet.sureBetTeam2,
            'Profit: ' + ((1 - (1 / surebet.coef1 + 1 / surebet.coef2)) * 100).toFixed(2),
            getSurebetId(surebet));
};

var checkSurebet = function(surebet) {
    var surebetId = getSurebetId(surebet);
    var surebetExist = false;
    for (var i = 0; i < historySurebets.length; i++) {
        var entry = historySurebets[i];
        var existingBetId = getSurebetId(entry);
        if (surebetId === existingBetId) {
            if (surebet.coef1 !== entry.coef1 || surebet.coef2 !== entry.coef2) {
                showSurebetNotification(surebet);
            }
            surebetExist = true;
            break;
        }
    }
    if (!surebetExist) {
        showSurebetNotification(surebet);
    }

    return surebetExist;
};

var updateSureBets = function() {
    $.ajax({
        url: 'surebets',
        type: 'POST',
        beforeSend: function(xhr) {
            xhr.setRequestHeader("Accept", "application/json");
            xhr.setRequestHeader("Content-Type", "application/json");
        },
        success: function(surebets) {
            $("#sureBets").empty();
            $("#sureBets").append('<h4>sure bets:</h4>');
            var table = '<table class="sure-bets">';
            var shouldPlaySound = false;
            surebets.forEach(function(entry) {
                var tableRow = getSurebetHtmlString(entry);
                table += tableRow;
                if (!checkSurebet(entry)) {
                    shouldPlaySound = true;
                }
            });
            table += '</table>';
            $("#sureBets").append(table);
            if (shouldPlaySound === true) {
                playSound();
            }

            historySurebets = surebets;
        }
    });
};

var updateTeams = function() {
    $.ajax({
        url: 'teams',
        type: 'POST',
        beforeSend: function(xhr) {
            xhr.setRequestHeader("Accept", "application/json");
            xhr.setRequestHeader("Content-Type", "application/json");
        },
        success: function(teams) {
            $("#teams").empty();
            $("#teams").append('<h4>teams:</h4>');
            teams.forEach(function(entry) {
                var htmlString = '<p>"' + entry + '"</p>';
                $("#teams").append(htmlString);
            });
        }
    });
};

var updateGames = function() {
    $.ajax({
        url: 'games',
        type: 'POST',
        beforeSend: function(xhr) {
            xhr.setRequestHeader("Accept", "application/json");
            xhr.setRequestHeader("Content-Type", "application/json");
        },
        success: function(games) {
            $("#games").empty();
            $("#games").append('<h4>games:</h4>');
            games.forEach(function(entry) {
                var htmlString = '<p>"' + entry + '"</p>';
                $("#games").append(htmlString);
            });
        }
    });
};

var updateBetTypes = function() {
    $.ajax({
        url: 'betTypes',
        type: 'POST',
        beforeSend: function(xhr) {
            xhr.setRequestHeader("Accept", "application/json");
            xhr.setRequestHeader("Content-Type", "application/json");
        },
        success: function(betTypes) {
            $("#betTypes").empty();
            $("#betTypes").append('<h4>bet types:</h4>');
            betTypes.forEach(function(entry) {
                var htmlString = '<p>"' + entry + '"</p>';
                $("#betTypes").append(htmlString);
            });
        }
    });
};

var onTimer = function() {
    updateSureBets();
    updateTeams();
    updateGames();
    updateBetTypes();
};

var onTimeout = function() {
    updateSureBets();
    updateTeams();
    updateGames();
    updateBetTypes();
    var oneSecond = 1000;
    var interval = 5 * oneSecond;
    var timer = setTimeout(onTimeout, interval);
};

var onDocumentReady = function() {
    requestPermission();
	$.ajaxSetup({ cache: false });
	onTimeout();
};

$(document).ready(onDocumentReady);