DROP TABLE IF EXISTS game;
DROP TABLE IF EXISTS bet_type;

DROP TABLE IF EXISTS game_alter_names;
DROP TABLE IF EXISTS bet_type_alter_names;

CREATE TABLE game (
    id        	 BIGINT PRIMARY KEY,
    name         VARCHAR_IGNORECASE(511) NOT NULL UNIQUE
);

CREATE TABLE bet_type (
    id        	 BIGINT PRIMARY KEY,
    name         VARCHAR_IGNORECASE(511) NOT NULL UNIQUE
);


CREATE TABLE game_alter_names (
    game_id             BIGINT,
    alter_name          VARCHAR_IGNORECASE(511) UNIQUE,
    PRIMARY KEY(game_id, alter_name)
);

CREATE TABLE bet_type_alter_names (
    bet_type_id         BIGINT,
    alter_name          VARCHAR_IGNORECASE(511) UNIQUE,
    PRIMARY KEY(bet_type_id, alter_name)
);

ALTER TABLE game_alter_names DROP CONSTRAINT fk_game_id_1 IF EXISTS;
ALTER TABLE bet_type_alter_names DROP CONSTRAINT fk_bet_type_id_1 IF EXISTS;

ALTER TABLE game_alter_names add constraint fk_game_id_1 foreign key (game_id) references game(id) on delete restrict on update restrict;
ALTER TABLE bet_type_alter_names add constraint fk_bet_type_id_1 foreign key (bet_type_id) references bet_type(id) on delete restrict on update restrict;