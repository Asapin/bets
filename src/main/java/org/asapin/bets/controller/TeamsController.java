package org.asapin.bets.controller;

import org.asapin.bets.dao.TeamDao;
import org.asapin.bets.parser.AbstractParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Set;

/**
 * Created by Asapin on 2016-02-02.
 */
@Controller
public class TeamsController {

    @Autowired
    private TeamDao teamDao;

    @RequestMapping(value = "/teams", method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Set<String> loadTeams() {
        return AbstractParser.TEAM_NAMES;
    }

    @RequestMapping(value = "/refreshTeamsNames", method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public boolean refreshTeamsNames() {
        teamDao.refreshTeamsNames();
        return true;
    }
}
