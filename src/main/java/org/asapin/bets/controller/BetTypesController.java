package org.asapin.bets.controller;

import org.asapin.bets.parser.AbstractParser;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Set;

/**
 * Created by Asapin on 2016-02-03.
 */
@Controller
public class BetTypesController {

    @RequestMapping(value = "/betTypes", method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Set<String> loadBetTypes() {
        return AbstractParser.BET_TYPES;
    }
}
