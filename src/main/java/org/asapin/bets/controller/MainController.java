package org.asapin.bets.controller;

import org.asapin.bets.domain.SureBet;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Collections;
import java.util.List;

/**
 * Created by Asapin on 10.01.2016.
 */
@Controller
public class MainController {

    public static List<SureBet> CACHED_SURE_BETS = Collections.EMPTY_LIST;
    public static List<SureBet> SURE_BETS = Collections.EMPTY_LIST;

    @RequestMapping("/index")
    public String loadMainPage(Model model) {
        return "/jsp/index.jsp";
    }

    @RequestMapping(value = "/surebets", method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<SureBet> loadGames() {
        return SURE_BETS;
    }
}
