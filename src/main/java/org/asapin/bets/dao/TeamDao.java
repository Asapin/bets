package org.asapin.bets.dao;

/**
 * Created by Asapin on 17.01.2016.
 */
public interface TeamDao {

    Integer getTeamIdByName(String name);

    String getTeamNameById(int id);

    void refreshTeamsNames();
}
