package org.asapin.bets.dao;

/**
 * Created by Asapin on 16.01.2016.
 */
public interface GameDao {

    Long getGameIdByName(String name);

    String getGameNameById(Long id);
}
