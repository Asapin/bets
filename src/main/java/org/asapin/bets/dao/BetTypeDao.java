package org.asapin.bets.dao;

/**
 * Created by Asapin on 2016-02-02.
 */
public interface BetTypeDao {

    Long getBetTypeIdByName(String name);

    String getBetTypeNameById(Long id);
}
