package org.asapin.bets.dao.impl;

import org.asapin.bets.dao.TeamDao;
import org.asapin.bets.parser.AbstractParser;
import org.asapin.bets.parser.GoogleSpreadsheetsParser;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.IntStream;

/**
 * Created by Asapin on 2016-02-02.
 */
@Repository
public class TeamDaoImpl extends AbstractDao implements TeamDao {

    private static ArrayList<String> TEAM_NAMES = new ArrayList<>();
    private static Map<String, Integer> ALTERNATIVE_TEAM_NAMES = new HashMap<>();

    private static final String TEAMS_NAMES_DOCUMENT_URL = "https://docs.google.com/spreadsheets/d/1zCjfylEwnYm-mucTGkFi6cmIjdGA8bShovDftW6qS8Y/pub?output=csv";

    private static boolean INITIALIZED = false;

    private boolean isInitialized() {
        return INITIALIZED;
    }

    private static void setInitialized(boolean initialized) {
        INITIALIZED = initialized;
    }

    private synchronized static void init() {
        GoogleSpreadsheetsParser parser = new GoogleSpreadsheetsParser();
        List<String> teamsNamesCsv = parser.getCSV(TEAMS_NAMES_DOCUMENT_URL);

        ArrayList<String> teamsNames = new ArrayList<>();
        Map<String, Integer> alternativeTeamsNames = new HashMap<>();

        teamsNamesCsv.stream().forEach(s -> {
            String[] teamNames = s.split(",");
            if (teamNames != null && teamNames.length > 0) {
                teamsNames.add(teamNames[0].trim());
                int index = teamsNames.indexOf(teamNames[0].trim());
                IntStream.range(1, teamNames.length).forEach(i -> {
                    alternativeTeamsNames.put(teamNames[i].trim(), index);
                });
            }
        });

        TEAM_NAMES = teamsNames;
        ALTERNATIVE_TEAM_NAMES = alternativeTeamsNames;
        AbstractParser.TEAM_NAMES.clear();

        setInitialized(true);
    }

    @Override
    public Integer getTeamIdByName(String name) {
        if (!isInitialized()) {
            init();
        }

        String teamName = name.trim();

        if (TEAM_NAMES.contains(teamName)) {
            return TEAM_NAMES.indexOf(teamName);
        }

        if (ALTERNATIVE_TEAM_NAMES.keySet().contains(teamName)) {
            return ALTERNATIVE_TEAM_NAMES.get(teamName);
        }

        return null;
    }

    @Override
    public String getTeamNameById(int id) {
        if (!isInitialized()) {
            init();
        }

        return TEAM_NAMES.get(id);
    }

    @Override
    public void refreshTeamsNames() {
        setInitialized(false);
    }
}
