package org.asapin.bets.dao.impl;

import org.asapin.bets.dao.GameDao;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Asapin on 2016-02-02.
 */
@Repository
public class GameDaoImpl extends AbstractDao implements GameDao {

    @Override
    public Long getGameIdByName(String name) {
        String sql = "select id " +
                "from game g " +
                "left join game_alter_names gn on g.id = gn.game_id " +
                "where g.name = ? or gn.alter_name = ?";

        List<Long> results = jdbcTemplate.queryForList(sql, Long.class, name, name);
        if (results.isEmpty()) {
            return null;
        }
        return results.get(0);
    }

    @Override
    public String getGameNameById(Long id) {
        String sql = "select name " +
                "from game g " +
                "where g.id = ?";

        List<String> results = jdbcTemplate.queryForList(sql, String.class, id);
        if (results.isEmpty()) {
            return null;
        }
        return results.get(0);
    }
}
