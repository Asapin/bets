package org.asapin.bets.dao.impl;

import org.asapin.bets.dao.BetTypeDao;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Asapin on 2016-02-02.
 */
@Repository
public class BetTypeDaoImpl extends AbstractDao implements BetTypeDao {

    @Override
    public Long getBetTypeIdByName(String name) {
        String sql = "select id " +
                "from bet_type bt " +
                "left join bet_type_alter_names btn on bt.id = btn.bet_type_id " +
                "where bt.name = ? or btn.alter_name = ?";

        List<Long> results = jdbcTemplate.queryForList(sql, Long.class, name, name);
        if (results.isEmpty()) {
            return null;
        }
        return results.get(0);
    }

    @Override
    public String getBetTypeNameById(Long id) {
        String sql = "select name " +
                "from bet_type bt " +
                "where bt.id = ?";

        List<String> results = jdbcTemplate.queryForList(sql, String.class, id);
        if (results.isEmpty()) {
            return null;
        }
        return results.get(0);
    }
}
