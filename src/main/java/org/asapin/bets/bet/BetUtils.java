package org.asapin.bets.bet;

import org.asapin.bets.domain.Bet;

import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by Asapin on 12.01.2016.
 */
public class BetUtils {

    public static Map<Long, List<Bet>> splitByGame(List<Bet> bets) {
        return bets.stream().collect(Collectors.groupingBy(Bet::getGameId));
    }

    public static boolean ifSameBet(Bet bet1, Bet bet2) {
        if (bet1.getGameId() != bet2.getGameId()) {
            return false;
        }
        if (bet1.getBookmaker() == bet2.getBookmaker()) {
            return false;
        }
        if (bet1.getBetTypeId() != bet2.getBetTypeId()) {
            return false;
        }
        if (bet1.getTeam1Id() != bet2.getTeam1Id() &&
                bet1.getTeam1Id() != bet2.getTeam2Id()) {
            return false;
        }
        if (bet1.getTeam2Id() != bet2.getTeam1Id() &&
                bet1.getTeam2Id() != bet2.getTeam2Id()) {
            return false;
        }
        if (Math.abs(ChronoUnit.HOURS.between(bet1.getGameTime(), bet2.getGameTime())) > 1) {
            return false;
        }
        return true;
    }

    public static boolean isAnyNull(Object... objects) {
        for (Object object: objects) {
            if (object == null) {
                return true;
            }
        }
        return false;
    }

    public static boolean isEqual(Number n1, Number n2) {
        if (n1 == null && n2 == null) {
            return true;
        }
        if (n1 == null) {
            return false;
        }
        if (n2 == null) {
            return false;
        }
        return n1.equals(n2);
    }
}
