package org.asapin.bets.bet;

import org.asapin.bets.dao.BetTypeDao;
import org.asapin.bets.dao.GameDao;
import org.asapin.bets.dao.TeamDao;
import org.asapin.bets.domain.Bet;
import org.asapin.bets.domain.SureBet;
import org.asapin.bets.parser.AbstractParser;
import org.asapin.bets.parser.EBettleParser;
import org.asapin.bets.parser.EGBParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.*;

/**
 * Created by Asapin on 13.01.2016.
 */
@Repository
public class SureBetsFinder {

    private static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss").withZone(ZoneId.of("Europe/Moscow"));

    @Autowired
    private EBettleParser eBettleParser;

    @Autowired
    private EGBParser egbParser;

    @Autowired
    private TeamDao teamDao;

    @Autowired
    private GameDao gameDao;

    @Autowired
    private BetTypeDao betTypeDao;

    public List<SureBet> findSureBets() {
        List<Bet> allBets = new ArrayList<>();
        for (AbstractParser parser: getParserList()) {
            allBets.addAll(parser.loadBets());
        }

        Map<Long, List<Bet>> betsByGame = BetUtils.splitByGame(allBets);

        List<SureBet> sureBets = new ArrayList<>();
        betsByGame.values().stream().forEach(
            bets -> {
                for (int i = 0; i < bets.size() - 1; i++) {
                    for (int j = i + 1; j < bets.size(); j++) {
                        if (BetUtils.ifSameBet(bets.get(i), bets.get(j))) {
                            List<SureBet> buf = getSureBets(bets.get(i), bets.get(j));
                            if (buf != null) {
                                sureBets.addAll(buf);
                            }
                        }
                    }
                }
            }
        );

        return sureBets;
    }

    public List<AbstractParser> getParserList() {
        List<AbstractParser> parserList = new ArrayList<>();
        parserList.add(eBettleParser);
        parserList.add(egbParser);
        return parserList;
    }

    public SureBet getSureBet(Bet bet1, Bet bet2, SurebetTeamOrder teamOrder) {
        String sureBetTeam1 = teamDao.getTeamNameById(bet1.getTeam1Id());
        String sureBetTeam2 = teamDao.getTeamNameById(bet1.getTeam2Id());
        String game = gameDao.getGameNameById(bet1.getGameId());
        String betType = betTypeDao.getBetTypeNameById(bet1.getBetTypeId());
        if (!BetUtils.isAnyNull(sureBetTeam1, sureBetTeam2, game, betType)) {
            SureBet sureBet = new SureBet();
            sureBet.setGame(game);
            sureBet.setBookmaker1Url(bet1.getBookmakerUrl());
            sureBet.setBookmaker2Url(bet2.getBookmakerUrl());

            switch (teamOrder) {
                case TEAM1_TEAM1:
                    sureBet.setSureBetTeam1(sureBetTeam1);
                    sureBet.setSureBetTeam2(sureBetTeam2);
                    sureBet.setCoef1(bet1.getCoef1());
                    sureBet.setCoef2(bet2.getCoef1());
                    break;
                case TEAM1_TEAM2:
                    sureBet.setSureBetTeam1(sureBetTeam1);
                    sureBet.setSureBetTeam2(sureBetTeam2);
                    sureBet.setCoef1(bet1.getCoef1());
                    sureBet.setCoef2(bet2.getCoef2());
                    break;
                case TEAM2_TEAM1:
                    sureBet.setSureBetTeam1(sureBetTeam2);
                    sureBet.setSureBetTeam2(sureBetTeam1);
                    sureBet.setCoef1(bet1.getCoef2());
                    sureBet.setCoef2(bet2.getCoef1());
                    break;
                case TEAM2_TEAM2:
                    sureBet.setSureBetTeam1(sureBetTeam2);
                    sureBet.setSureBetTeam2(sureBetTeam1);
                    sureBet.setCoef1(bet1.getCoef2());
                    sureBet.setCoef2(bet2.getCoef2());
                    break;
            }

            sureBet.setLocalTimeString(dateTimeFormatter.format(bet1.getGameTime()));
            sureBet.setBetType(betType);
            return sureBet;
        }
        return null;
    }

    public List<SureBet> getSureBets(Bet bet1, Bet bet2) {
        List<SureBet> sureBets = new ArrayList<>();
        boolean sameOrder = false;

        if (bet1.getTeam1Id() == bet2.getTeam1Id()) {
            sameOrder = true;
        }

        if (sameOrder) {
            if (1/bet1.getCoef1() + 1/bet2.getCoef2() < 1) {
                SureBet sureBet = getSureBet(bet1, bet2, SurebetTeamOrder.TEAM1_TEAM2);
                if (sureBet != null) {
                    sureBets.add(sureBet);
                }
            }

            if (1/bet1.getCoef2() + 1/bet2.getCoef1() < 1) {
                SureBet sureBet = getSureBet(bet1, bet2, SurebetTeamOrder.TEAM2_TEAM1);
                if (sureBet != null) {
                    sureBets.add(sureBet);
                }
            }
        } else {
            if (1/bet1.getCoef1() + 1/bet2.getCoef1() < 1) {
                SureBet sureBet = getSureBet(bet1, bet2, SurebetTeamOrder.TEAM1_TEAM1);
                if (sureBet != null) {
                    sureBets.add(sureBet);
                }
            }

            if (1/bet1.getCoef2() + 1/bet2.getCoef2() < 1) {
                SureBet sureBet = getSureBet(bet1, bet2, SurebetTeamOrder.TEAM2_TEAM2);
                if (sureBet != null) {
                    sureBets.add(sureBet);
                }
            }
        }

        return sureBets;
    }
}
