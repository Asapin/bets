package org.asapin.bets.bet;

/**
 * Created by Asapin on 2016-04-15.
 */
public enum SurebetTeamOrder {
    TEAM1_TEAM1,
    TEAM1_TEAM2,
    TEAM2_TEAM1,
    TEAM2_TEAM2
}
