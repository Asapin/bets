package org.asapin.bets.domain;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Created by Asapin on 13.01.2016.
 */
@Getter
@Setter
@EqualsAndHashCode
@ToString
public class SureBet {
    private String sureBetTeam1;
    private String sureBetTeam2;
    private String game;
    private String bookmaker1Url;
    private String bookmaker2Url;
    private double coef1;
    private double coef2;
    private String localTimeString;
    private String betType;
}
