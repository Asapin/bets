package org.asapin.bets.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.net.URL;
import java.util.List;
import java.util.Map;

/**
 * Created by Asapin on 2016-04-01.
 */
@Getter
@Setter
@ToString
public class HttpResponse {
    private int responseCode;
    private String content;
    private Map<String, List<String>> headers;
    private URL requestUrl;
}
