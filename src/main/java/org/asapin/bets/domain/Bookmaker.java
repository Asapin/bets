package org.asapin.bets.domain;

import lombok.Getter;

/**
 * Created by Asapin on 2016-04-19.
 */
@Getter
public enum Bookmaker {
    EGB,
    EBETTLE;
}
