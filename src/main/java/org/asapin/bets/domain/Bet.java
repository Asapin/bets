package org.asapin.bets.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.time.ZonedDateTime;

/**
 * Created by Asapin on 10.01.2016.
 */
@Getter
@Setter
@ToString
public class Bet {
    private long gameId;
    private int team1Id;
    private int team2Id;
    private long betTypeId;
    private double coef1;
    private double coef2;
    private ZonedDateTime gameTime;
    private Bookmaker bookmaker;
    private String bookmakerUrl;
}
