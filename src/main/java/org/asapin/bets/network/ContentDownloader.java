package org.asapin.bets.network;

import org.asapin.bets.domain.HttpResponse;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpCookie;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.*;
import java.util.zip.GZIPInputStream;

/**
 * Created by Asapin on 10.01.2016.
 */
public class ContentDownloader {

    public static HttpResponse downloadContentByURL(String urlString, Map<String, String> headers, Set<HttpCookie> cookies) throws IOException {
        return downloadContentByURL(urlString, headers, cookies, true);
    }

    public static HttpResponse downloadContentByURL(String urlString, Map<String, String> headers, Set<HttpCookie> cookies, boolean autoRedirect) throws IOException {
        URL url = new URL(urlString);
        HttpURLConnection conn = null;
        StringBuilder sb = new StringBuilder();
        BufferedReader rd = null;

        try {
            conn = (HttpURLConnection) url.openConnection();

            NetUtils.setHeaders(conn, headers);
            NetUtils.setCookies(conn, cookies);

            conn.setRequestMethod("GET");
            conn.setInstanceFollowRedirects(autoRedirect);

            HttpResponse response = new HttpResponse();
            response.setResponseCode(conn.getResponseCode());
            response.setHeaders(conn.getHeaderFields());
            response.setRequestUrl(url);

            if (cookies != null) {
                Set<HttpCookie> requestCookies = NetUtils.extractCookies(conn.getHeaderFields());
                cookies.addAll(requestCookies);
            }

            if ("gzip".equals(conn.getContentEncoding())) {
                rd = new BufferedReader(new InputStreamReader(new GZIPInputStream(conn.getInputStream()), "UTF-8"));
            } else if (conn.getResponseCode() >= 200 && conn.getResponseCode() < 400) {
                rd = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
            } else {
                rd = new BufferedReader(new InputStreamReader(conn.getErrorStream(), "UTF-8"));
            }

            String line;
            while ((line = rd.readLine()) != null) {
                sb.append(line + "\n");
            }
            response.setContent(sb.toString());

            if ((response.getResponseCode() == 301 || response.getResponseCode() == 302) &&
                    response.getHeaders().get("Location") != null) {
                response = downloadContentByURL(response.getHeaders().get("Location").get(0), headers, cookies);
            }
            return response;
        } finally {
            if (rd != null) {
                rd.close();
            }

            if (conn != null) {
                conn.disconnect();
            }
        }
    }
}
