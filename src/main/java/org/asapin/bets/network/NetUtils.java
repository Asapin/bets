package org.asapin.bets.network;

import java.net.HttpCookie;
import java.net.HttpURLConnection;
import java.util.*;

/**
 * Created by Asapin on 2016-04-02.
 */
public class NetUtils {

    public static void setHeaders(HttpURLConnection conn, Map<String, String> headers) {
        if (headers == null) {
            return;
        }

        headers.put("Referer", conn.getURL().getProtocol() + "://" + conn.getURL().getAuthority());
        headers.entrySet().forEach(header -> conn.setRequestProperty(header.getKey(), header.getValue()));
    }

    public static void setCookies(HttpURLConnection conn, Collection<HttpCookie> cookies) {
        if (cookies == null) {
            return;
        }

        Iterator<HttpCookie> iter = cookies.iterator();
        while (iter.hasNext()) {
            HttpCookie cookie = iter.next();
            if (cookie.hasExpired()) {
                iter.remove();
            } else {
                conn.addRequestProperty("Cookie", cookie.getName() + "=" + cookie.getValue());
            }
        }
    }

    public static Set<HttpCookie> extractCookies(Map<String, List<String>> headers) {
        List<String> requestCookies = headers.get("Set-Cookie");
        if (requestCookies == null || requestCookies.isEmpty()) {
            return Collections.emptySet();
        }

        Set<HttpCookie> cookies = new HashSet<>();
        requestCookies.stream()
                .filter(s -> !s.isEmpty())
                .forEach(s -> {
                    Collection<HttpCookie> httpCookies = HttpCookie.parse(s);
                    if (httpCookies != null) {
                        cookies.addAll(httpCookies);
                    }
                });

        return cookies;
    }

}
