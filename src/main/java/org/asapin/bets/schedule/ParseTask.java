package org.asapin.bets.schedule;

import org.asapin.bets.domain.SureBet;
import org.asapin.bets.bet.SureBetsFinder;
import org.asapin.bets.controller.MainController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by Asapin on 2016-02-06.
 */
@Component
public class ParseTask {

    @Autowired
    private SureBetsFinder sureBetsFinder;

    @Scheduled(fixedDelay = 20 * 1000)
    public void parseBets() {
        List<SureBet> bets = sureBetsFinder.findSureBets();
        MainController.CACHED_SURE_BETS = MainController.SURE_BETS;
        MainController.SURE_BETS = bets;
    }
}
