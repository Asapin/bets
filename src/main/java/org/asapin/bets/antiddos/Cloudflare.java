package org.asapin.bets.antiddos;

import org.asapin.bets.domain.HttpResponse;
import org.asapin.bets.network.ContentDownloader;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.FormElement;
import org.mozilla.javascript.Context;
import org.mozilla.javascript.Scriptable;
import org.springframework.util.StringUtils;

import java.io.IOException;
import java.net.HttpCookie;
import java.time.Duration;
import java.time.Instant;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * Created by Asapin on 2016-04-01.
 */
public class Cloudflare {

    private static final Pattern SCRIPT_PATTERN = Pattern.compile("setTimeout\\(function\\(\\)\\{(.+)}, 4000\\)", Pattern.DOTALL);
    private static final Pattern VARIABLE_PATTERN = Pattern.compile("var t,r,a,f,[ ]+(.+)", Pattern.DOTALL);

    public static void bypass(HttpResponse httpResponse, Map<String, String> headers, Set<HttpCookie> cookies) throws IOException, InterruptedException {
        HttpResponse mainPageResponse = ContentDownloader.downloadContentByURL(
                httpResponse.getRequestUrl().getProtocol() + "://" + httpResponse.getRequestUrl().getHost(),
                headers, cookies);

        Instant bypassStartTime = Instant.now();

        List<String> refreshHeaders = mainPageResponse.getHeaders().get("Refresh");
        if (refreshHeaders == null || refreshHeaders.isEmpty()) {
            return;
        }

        String refreshHeader = refreshHeaders.get(0);
        String[] headerParts = refreshHeader.split(";");
        Integer refreshDelay = Integer.parseInt(headerParts[0]);

        String challengeJs = extractChallengeJs(mainPageResponse);
        Integer challengeResult = calculateChallengeResult(challengeJs);

        if (challengeResult == null) {
            return;
        }

        challengeResult += mainPageResponse.getRequestUrl().getHost().length();

        Document html = Jsoup.parse(mainPageResponse.getContent());
        FormElement challengeForm = (FormElement) html.getElementById("challenge-form");
        String actionUrl = challengeForm.attr("action");

        StringBuilder urlBuilder = new StringBuilder();
        urlBuilder.append(mainPageResponse.getRequestUrl())
                .append(actionUrl)
                .append("?");

        Integer finalChallengeResult = challengeResult;

        challengeForm.elements().stream()
                .filter(e -> e.val() == null || e.val().isEmpty())
                .forEach(element -> element.val(finalChallengeResult.toString()));

        urlBuilder.append(StringUtils.collectionToDelimitedString(challengeForm.formData(), "&"));

        Instant bypassEndTime = Instant.now();
        long bypassTime = Duration.between(bypassStartTime, bypassEndTime).toMillis();
        long sleepTime = refreshDelay * 1000 - bypassTime;
        if (sleepTime > 0) {
            Thread.sleep(sleepTime);
        }

        String cacheControlHeader = headers.get("Cache-Control");
        if (cacheControlHeader != null) {
            headers.remove(cacheControlHeader);
        }

        ContentDownloader.downloadContentByURL(urlBuilder.toString(), headers, cookies, false);
        if (cacheControlHeader != null) {
            headers.put("Cache-Control", cacheControlHeader);
        }
    }

    public static boolean isCloudflareProtected(HttpResponse response) {
        boolean isProtected = false;

        if (response.getResponseCode() >= 500 && response.getHeaders().get("Refresh") != null) {
            List<String> servers = response.getHeaders().get("Server");
            if (servers != null) {
                isProtected = servers.stream().anyMatch("cloudflare-nginx"::equals);
            }
        }

        return isProtected;
    }

    private static String extractChallengeJs(HttpResponse httpResponse) {
        Matcher operationSearch = SCRIPT_PATTERN.matcher(httpResponse.getContent());
        if (!operationSearch.find()) {
            return "";
        }

        String rawOperation = operationSearch.group(1);
        String[] operations = rawOperation.split(";");
        operations = StringUtils.trimArrayElements(operations);

        Matcher variableMatcher = VARIABLE_PATTERN.matcher(operations[0]);
        if (!variableMatcher.find()) {
            return "";
        }

        String[] variable = variableMatcher.group(1).split("=", 2);
        String variableName = variable[0];

        List<String> challengeOperations = Arrays.stream(operations)
                .filter(s -> s.startsWith(variableName))
                .collect(Collectors.toList());

        return "var " + variableMatcher.group(1) + ";" + StringUtils.collectionToDelimitedString(challengeOperations, ";") + ";";
    }

    private static Integer calculateChallengeResult(String challengeJs) {
        Context rhino = Context.enter();
        Scriptable scope = rhino.initSafeStandardObjects();
        Object challengeResult = rhino.evaluateString(scope, challengeJs, "CloudFlare JS Challenge", 1, null);

        if (challengeResult instanceof Number) {
            return ((Number) challengeResult).intValue();
        }

        return null;
    }
}
