package org.asapin.bets.parser;

import org.asapin.bets.domain.Bet;
import org.asapin.bets.bet.BetUtils;
import org.asapin.bets.domain.Bookmaker;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.stereotype.Repository;

import java.net.HttpCookie;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.*;

/**
 * Created by Asapin on 10.01.2016.
 */
@Repository
public class EGBParser extends AbstractParser {

    private static final String URL = "http://egb.com/ajax.php?act=UpdateTableBets&ajax=update&fg=1&ind=tables&limit=0&st=0&type=modules&ut=0";
    private static final String BOOKMAKER_NAME = "EGB";

    private static final Map<String, String> headers = new HashMap<>();
    private static final Set<HttpCookie> cookies = new HashSet<>();

    @Override
    protected String getUrl() {
        return URL;
    }

    @Override
    protected String getBookmakerName() {
        return BOOKMAKER_NAME;
    }

    @Override
    protected List<Bet> parsePage(String pageContent) {
        if (pageContent == null || pageContent.isEmpty()) {
            return Collections.emptyList();
        }

        JSONObject jsonObject = new JSONObject(pageContent);
        JSONArray jsonBets = jsonObject.getJSONArray("bets");
        List<Bet> result = new ArrayList<>();
        for (int i = 0; i < jsonBets.length(); i++) {
            List<Bet> bets = getBetFromJSON(jsonBets.getJSONObject(i));
            result.addAll(bets);
        }
        return result;
    }

    @Override
    protected Map<String, String> getHeaders() {
        if (headers.isEmpty()) {
            headers.put("Host", "egb.com");
            headers.put("Connection", "keep-alive");
            headers.put("Cache-Control", "max-age=0");
            headers.put("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8");
            headers.put("Upgrade-Insecure-Requests", "1");
            headers.put("User-Agent", "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.110 Safari/537.36");
            headers.put("DNT", "1");
            headers.put("Referer", "http://egb.com/registration");
            headers.put("Accept-Encoding", "gzip, deflate, sdch");
            headers.put("Accept-Language", "ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4");
        }
        return headers;
    }

    @Override
    protected Set<HttpCookie> getCookies() {
        return cookies;
    }

    private List<Bet> getBetFromJSON(JSONObject jsonBet) {
        Long gameId = getGameIdByName(jsonBet.getString("game"));
        Integer teamId1 = getTeamIdByName(jsonBet.getJSONObject("gamer_1").getString("nick").toLowerCase().replace("(live)", ""));
        Integer teamId2 = getTeamIdByName(jsonBet.getJSONObject("gamer_2").getString("nick").toLowerCase().replace("(live)", ""));
        String betId = jsonBet.getString("id");
        if (betId == null) {
            betId = "";
        }

        if (BetUtils.isAnyNull(gameId, teamId1, teamId2)) {
            return Collections.EMPTY_LIST;
        }

        if (!jsonBet.isNull("coef_draw")) {
            return Collections.EMPTY_LIST;
        }

        Instant instant = Instant.ofEpochSecond(jsonBet.getLong("date"));
        ZonedDateTime matchTime = ZonedDateTime.ofInstant(instant, ZoneId.systemDefault());
        ZonedDateTime currentTime = ZonedDateTime.now(ZoneOffset.UTC);
        if (matchTime.isBefore(currentTime)) {
            return Collections.EMPTY_LIST;
        }

        List<Bet> bets = new ArrayList<>();

        Bet bet = new Bet();
        bet.setGameId(gameId);
        bet.setTeam1Id(teamId1);
        bet.setTeam2Id(teamId2);
        bet.setGameTime(matchTime);
        bet.setCoef1(jsonBet.getDouble("coef_1"));
        bet.setCoef2(jsonBet.getDouble("coef_2"));
        bet.setBookmaker(Bookmaker.EGB);
        bet.setBookmakerUrl("http://egb.com/tables#" + betId);
        bet.setBetTypeId(1);

        bets.add(bet);

        List<Bet> nestedBets = getNestedBets(jsonBet);
        fillNestedBets(bet, nestedBets);

        bets.addAll(nestedBets);
        return bets;
    }

    private List<Bet> getNestedBets(JSONObject jsonBet) {
        if (jsonBet.getInt("nested_bets_count") == 0) {
            return Collections.EMPTY_LIST;
        }
        List<Bet> nestedBets = new ArrayList<>();
        JSONArray jsonArray = jsonBet.getJSONArray("nb_arr");
        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject jsonNestedBet = jsonArray.getJSONObject(i);

            String betType1 = jsonNestedBet.getJSONObject("gamer_1").getString("nick");
            String betType2 = jsonNestedBet.getJSONObject("gamer_2").getString("nick");
            if (!betType1.equalsIgnoreCase(betType2)) {
                continue;
            }

            Long betTypeId1 = getBetTypeIdByName(betType1);
            Long betTypeId2 = getBetTypeIdByName(betType2);
            if (BetUtils.isAnyNull(betTypeId1, betTypeId2)) {
                continue;
            }

            if (betTypeId1.compareTo(betTypeId2) != 0) {
                continue;
            }

            if (!jsonNestedBet.isNull("coef_draw")) {
                continue;
            }

            Bet nestedBet = new Bet();
            nestedBet.setCoef1(jsonNestedBet.getDouble("coef_1"));
            nestedBet.setCoef2(jsonNestedBet.getDouble("coef_2"));
            nestedBet.setBetTypeId(betTypeId1);
            nestedBets.add(nestedBet);
        }
        return nestedBets;
    }

    private void fillNestedBets(Bet bet, List<Bet> nestedBets) {
        for (Bet nestedBet: nestedBets) {
            nestedBet.setGameId(bet.getGameId());
            nestedBet.setTeam1Id(bet.getTeam1Id());
            nestedBet.setTeam2Id(bet.getTeam2Id());
            nestedBet.setGameTime(bet.getGameTime());
            nestedBet.setBookmakerUrl(bet.getBookmakerUrl());
        }
    }
}
