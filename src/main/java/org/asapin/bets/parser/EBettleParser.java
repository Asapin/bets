package org.asapin.bets.parser;

import org.asapin.bets.domain.Bet;
import org.asapin.bets.bet.BetUtils;
import org.asapin.bets.domain.Bookmaker;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.stereotype.Repository;

import java.net.HttpCookie;
import java.text.ParseException;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

/**
 * Created by Asapin on 10.01.2016.
 */
@Repository
public class EBettleParser extends AbstractParser {

    private static final String URL = "https://ebettle.com/api/sportmatch/Get?sportID=2357";
    private static final DateTimeFormatter DATE_FORMAT = DateTimeFormatter.ISO_OFFSET_DATE_TIME;
    private static final String BOOKMAKER_NAME = "EBettle";

    private static final Map<String, String> headers = new HashMap<>();
    private static final Set<HttpCookie> cookies = new HashSet<>();

    @Override
    protected String getUrl() {
        return URL;
    }

    @Override
    protected String getBookmakerName() {
        return BOOKMAKER_NAME;
    }

    @Override
    protected List<Bet> parsePage(String pageContent) {
        if (pageContent == null || pageContent.isEmpty()) {
            return Collections.emptyList();
        }

        JSONArray bets = new JSONArray(pageContent);
        List<Bet> result = new ArrayList<>();
        for (int i = 0; i < bets.length(); i++) {
            try {
                Bet bet = getBetFromJSON(bets.getJSONObject(i));
                if (bet != null) {
                    result.add(bet);
                }
            } catch (Exception e) {
                LOGGER.error("Can't parse bet: " + bets.getJSONObject(i).toString(), e);
            }
        }
        return result;
    }

    @Override
    protected Map<String, String> getHeaders() {
        if (headers.isEmpty()) {
            headers.put("authority", "ebettle.com");
            headers.put("path", "/api/sportmatch/Get?sportID=2357");
            headers.put("scheme", "https");
            headers.put("accept", "application/json, text/plain, */*");
            headers.put("accept-encoding", "gzip, deflate, sdch");
            headers.put("accept-language", "ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4");
            headers.put("dnt", "1");
            headers.put("referer", "https://ebettle.com/sport/esports");
            headers.put("user-agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.106 Safari/537.36");
        }
        return headers;
    }

    @Override
    protected Set<HttpCookie> getCookies() {
        return cookies;
    }

    private Bet getBetFromJSON(JSONObject jsonBet) throws ParseException {
        Long gameId = getGameIdByName(jsonBet.getJSONObject("Category").getString("Name"));
        Integer betId = jsonBet.getInt("ID");

        Long betTypeId1 = 0L;
        Long betTypeId2 = 0L;

        String teamName1 = jsonBet.getString("OriginalHomeTeamName").toLowerCase();
        int openBracesPos = teamName1.lastIndexOf('(');
        int closeBracesPos = teamName1.lastIndexOf(')');
        if (openBracesPos != -1 && closeBracesPos > openBracesPos) {
            String bracesContent = teamName1.substring(openBracesPos + 1, closeBracesPos);
            teamName1 = teamName1.substring(0, openBracesPos).trim();
            if (!bracesContent.equals("live")) {
                betTypeId1 = getBetTypeIdByName(bracesContent);
            }
        }
        Integer teamId1 = getTeamIdByName(teamName1);

        String teamName2 = jsonBet.getString("OriginalAwayTeamName").toLowerCase();
        openBracesPos = teamName2.lastIndexOf('(');
        closeBracesPos = teamName2.lastIndexOf(')');
        if (openBracesPos != -1 && closeBracesPos > openBracesPos) {
            String bracesContent = teamName2.substring(openBracesPos + 1, closeBracesPos);
            teamName2 = teamName2.substring(0, openBracesPos).trim();
            if (!bracesContent.equals("live")) {
                betTypeId2 = getBetTypeIdByName(bracesContent);
            }
        }
        Integer teamId2 = getTeamIdByName(teamName2);

        if (!BetUtils.isEqual(betTypeId1, betTypeId2)) {
            return null;
        }

        Long betTypeId = 0L;
        if (betTypeId1 == null || betTypeId1 == 0) {
            betTypeId = getBetTypeIdByName(jsonBet.getJSONObject("PreviewMarket").getString("Name"));
        } else {
            betTypeId = betTypeId1;
        }

        if (BetUtils.isAnyNull(gameId, teamId1, teamId2, betTypeId)) {
            return null;
        }

        if (jsonBet.getJSONArray("PreviewOdds").length() != 2) {
            return null;
        }

        ZonedDateTime matchTime = ZonedDateTime.parse(jsonBet.getString("DateOfMatch") + "Z", DATE_FORMAT);
        ZonedDateTime currentTime = ZonedDateTime.now(ZoneOffset.UTC);
        if (matchTime.isBefore(currentTime)) {
            return null;
        }

        Bet bet = new Bet();
        bet.setGameId(gameId);
        bet.setTeam1Id(teamId1);
        bet.setTeam2Id(teamId2);
        bet.setGameTime(matchTime);
        bet.setCoef1(jsonBet.getJSONArray("PreviewOdds").getJSONObject(0).getDouble("Value"));
        bet.setCoef2(jsonBet.getJSONArray("PreviewOdds").getJSONObject(1).getDouble("Value"));
        bet.setBookmaker(Bookmaker.EBETTLE);
        bet.setBookmakerUrl("https://ebettle.com/sport/match/" + betId);
        bet.setBetTypeId(betTypeId);
        return bet;
    }
}
