package org.asapin.bets.parser;

import org.apache.log4j.Logger;
import org.asapin.bets.domain.HttpResponse;
import org.asapin.bets.network.ContentDownloader;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Created by Asapin on 4/23/2016.
 */
public class GoogleSpreadsheetsParser {

    protected static final Logger LOGGER = Logger.getLogger(GoogleSpreadsheetsParser.class);

    public List<String> getCSV(String urlString) {
        try {
            HttpResponse response = ContentDownloader.downloadContentByURL(urlString, null, null);
            if (response.getResponseCode() < 200 || response.getResponseCode() >= 400) {
                return Collections.emptyList();
            }

            String[] rows = response.getContent().split("\n");
            return Arrays.asList(rows);
        } catch (IOException e) {
            LOGGER.error("Error while trying to parse google spreadsheets", e);
        }

        return Collections.emptyList();
    }
}
