package org.asapin.bets.parser;

import org.apache.log4j.Logger;
import org.asapin.bets.antiddos.Cloudflare;
import org.asapin.bets.domain.Bet;
import org.asapin.bets.dao.BetTypeDao;
import org.asapin.bets.dao.GameDao;
import org.asapin.bets.dao.TeamDao;
import org.asapin.bets.domain.HttpResponse;
import org.asapin.bets.network.ContentDownloader;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.net.HttpCookie;
import java.util.*;

/**
 * Created by Asapin on 10.01.2016.
 */
public abstract class AbstractParser {

    @Autowired
    private GameDao gameDao;

    @Autowired
    private TeamDao teamDao;

    @Autowired
    private BetTypeDao betTypeDao;

    protected static final Logger LOGGER = Logger.getLogger(AbstractParser.class);

    public static Set<String> GAME_NAMES = new HashSet<>();
    public static Set<String> TEAM_NAMES = new HashSet<>();
    public static Set<String> BET_TYPES = new HashSet<>();

    protected abstract String getUrl();
    protected abstract String getBookmakerName();
    protected abstract List<Bet> parsePage(String pageContent);
    protected abstract Map<String, String> getHeaders();
    protected abstract Set<HttpCookie> getCookies();

    public List<Bet> loadBets() {
        try {
            HttpResponse response = ContentDownloader.downloadContentByURL(getUrl(), getHeaders(), getCookies());
            if (response.getResponseCode() < 200 || response.getResponseCode() >= 400) {
                if (Cloudflare.isCloudflareProtected(response)) {
                    Cloudflare.bypass(response, getHeaders(), getCookies());
                }
                return Collections.emptyList();
            } else {
                return parsePage(response.getContent());
            }
        } catch (IOException | InterruptedException e) {
            LOGGER.error("Can't get access to site: " + getUrl(), e);
            return Collections.emptyList();
        }
    }

    protected Long getGameIdByName(String name) {
        String trimmedName = name.toLowerCase().trim();
        Long gameId = gameDao.getGameIdByName(trimmedName);
        if (gameId == null) {
            GAME_NAMES.add("[" + getBookmakerName() + "]" + trimmedName);
            LOGGER.info("Couldn't find game id by name: " + trimmedName);
        }
        return gameId;
    }

    protected Integer getTeamIdByName(String name) {
        String trimmedName = name.toLowerCase().trim();
        Integer teamId = teamDao.getTeamIdByName(trimmedName);
        if (teamId == null) {
            TEAM_NAMES.add("[" + getBookmakerName() + "]" + trimmedName);
            LOGGER.info("Couldn't find team id by name: " + trimmedName);
        }
        return teamId;
    }

    protected Long getBetTypeIdByName(String name) {
        String trimmedName = name.toLowerCase().trim();
        Long betTypeId = betTypeDao.getBetTypeIdByName(trimmedName);
        if (betTypeId == null) {
            BET_TYPES.add("[" + getBookmakerName() + "]" + trimmedName);
            LOGGER.info("Couldn't find bet type id by name: " + trimmedName);
        }
        return betTypeId;
    }
}
